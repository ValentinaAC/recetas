package com.example.recetas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//Cada vez que se hace algo relevante se hace el paso del "commit" y despues el "push"

public class MainActivity extends AppCompatActivity {

    private EditText etAlimento;
    private Button btBuscar, btLimpiar;
    private TextView tvIngredientes, tvNutricional;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.etAlimento = (EditText) findViewById(R.id.etAlimento);
        this.btBuscar = (Button) findViewById(R.id.btBuscar);
        this.tvIngredientes = (TextView) findViewById(R.id.tvIngredientes);
        this.tvNutricional = (TextView) findViewById(R.id.tvNutricional);
        this.btLimpiar = (Button) findViewById(R.id.btLimpiar);

        this.btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String consulta = etAlimento.getText().toString();

                String url = "https://api.edamam.com/search?q=" +
                        consulta +"&app_id=337eb895&app_key=18965abb73e73479d6a126088d75c69c&from=0&to=2";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            ArrayList<String> nombres = new ArrayList<>();
                            JSONArray recetas = respuestaJSON.getJSONArray("hits");
                            JSONObject ingredientes = recetas.getJSONObject(0);
                            JSONObject recii = ingredientes.getJSONObject("recipe");
                            JSONArray ingret = recii.getJSONArray("ingredients");
                            for(int i=0; i<ingret.length(); i++){
                                JSONObject elemento = ingret.getJSONObject(i);
                                nombres.add(elemento.getString("text")+ "\n");
                                tvIngredientes.setText("Los ingredientes son: "+ "\n" +nombres);

                            }

                            ArrayList<String> informacion = new ArrayList<>();
                            JSONArray nutri = recii.getJSONArray("digest");
                            for(int i=0; i<nutri.length(); i++){
                                JSONObject objeto = nutri.getJSONObject(i);
                                informacion.add(objeto.getString("label"));
                                informacion.add(objeto.getString("total")+ "\n");

                                tvNutricional.setText("Información nutricional: "+ "\n" +informacion);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }

                );
                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);

            }
        });

        this.btLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvIngredientes.setText("");
                tvNutricional.setText("");
                etAlimento.setText("");

            }
        });





    }


}

